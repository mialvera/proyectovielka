/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto.vielka;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import ventanas.Principal;

/**
 *
 * @author 
 */
public class ProyectoVielka extends Application {
    
    @Override
    public void start(Stage primaryStage) {
        Principal principal = new Principal();
        BorderPane root = principal.ventana();
        
        
        Scene scene = new Scene(root, 650, 500);
        
        primaryStage.setTitle("Alcancia!");
        primaryStage.setScene(scene);
        primaryStage.show();
        
        
        primaryStage.setOnCloseRequest(closeEvent -> {
            try {
                Principal.guardar();
            } catch (IOException ex) {
                Logger.getLogger(ProyectoVielka.class.getName()).log(Level.SEVERE, null, ex);
            }
        }); 
        

//       Runnable r = new Runnable() {
//           @Override
//           public void run() {
//               while(true){
//                   System.out.println("Hola");
//                   
//                   try {
//                       Thread.sleep(2000);
//                   } catch (InterruptedException ex) {
//                       Logger.getLogger(ProyectoVielka.class.getName()).log(Level.SEVERE, null, ex);
//                   }
//               }
//           }
//       };

//       Correr c1 = new Correr("c1");
//       Correr c2 = new Correr("c2");
//       
//       c1.start();
//       c2.start();
//       Thread t = new Thread(c1);
//       t.start();
//       
//        try {
//                Thread.sleep(2000);
//        } catch (InterruptedException ex) {
//                Logger.getLogger(Correr.class.getName()).log(Level.SEVERE, null, ex);
//        }
//                   
//       Thread t2 = new Thread(c2);
//       t2.start();
       
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
