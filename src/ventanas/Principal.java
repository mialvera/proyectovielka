/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ventanas;

import java.awt.event.WindowAdapter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 *
 * @author 
 */
public class Principal {
    BorderPane ventana;
    HBox monedas;
    HBox alcancia;
    VBox total;
    Label ahorro;
    Label centavoC;
    Label cincoC;
    Label diezC;
    Label veinticincoC;
    Label cincuentaC;
    Label dolarC;
    HBox cantidadMoneda;
    double dineroTotal;
    
    Moneda[] monedasArreglo;
    ImageView chancho;
    
    
    static Map<Moneda,Integer> monedaMapa= new HashMap<>();
    
    
    public Principal(){
        //Inicializar pantalla
        ventana= new BorderPane();
        monedas = new HBox();
        alcancia = new HBox();
        total = new VBox();
        ahorro = new Label("$0.00");
        cantidadMoneda = new HBox();
        
     
        total.getChildren().addAll(monedas,cantidadMoneda,alcancia);
        ventana.setCenter(total);
        
        Label titulo = new Label("Alcancía");
        ventana.setTop(titulo);
        
        imagenes(monedas);
        imagenes2(alcancia);
        
      //Inicializar cantidad de monedas
        centavoC= new Label("0");
        cincoC= new Label("0");
        diezC= new Label("0");
        veinticincoC= new Label("0");
        cincuentaC= new Label("0");
        dolarC= new Label("0");
        
        cantidadMoneda.getChildren().addAll(centavoC, cincoC, diezC, 
                veinticincoC, cincuentaC, dolarC);
        cantidadMoneda.setSpacing(90);
        cantidadMoneda.setAlignment(Pos.CENTER_LEFT);
        
        //Inicializar Monedas
        monedasArreglo = new Moneda[6];
                
        monedasArreglo[0] = new Moneda(0.01, "centavo");
        monedasArreglo[1] = new Moneda(0.05, "cinco");
        monedasArreglo[2] = new Moneda(0.1, "diez");
        monedasArreglo[3] = new Moneda(0.25, "veinticinco");
        monedasArreglo[4] = new Moneda(0.5, "cincuenta");
        monedasArreglo[5] = new Moneda(1.0, "dolar");
        
        monedaMapa.put(monedasArreglo[0],0);
        monedaMapa.put(monedasArreglo[1],0);
        monedaMapa.put(monedasArreglo[2],0);
        monedaMapa.put(monedasArreglo[3],0);
        monedaMapa.put(monedasArreglo[4],0);
        monedaMapa.put(monedasArreglo[5],0);
        

        inicializarValores();
    
        
    }
    
    private void imagenes(HBox monedas){
        
        ImageView cent= imagenesMonedas("/imagenes/centavo.png",100);
        ImageView cinco= imagenesMonedas("/imagenes/cinco.png",100);
        ImageView diez= imagenesMonedas("/imagenes/diez.png",100);
        ImageView veinticinco= imagenesMonedas("/imagenes/veinticinco.png",100);
        ImageView cincuenta= imagenesMonedas("/imagenes/cincuenta.png",100);
        ImageView dolar= imagenesMonedas("/imagenes/dolar.png",100);
        
        cent.setOnMouseClicked(e -> monedaCentavo(centavoC));
        cinco.setOnMouseClicked(e -> monedaCinco(cincoC));
        diez.setOnMouseClicked(e -> monedaDiez(diezC));
        veinticinco.setOnMouseClicked(e -> monedaVeinticinco(veinticincoC));
        cincuenta.setOnMouseClicked(e -> monedaCincuenta(cincuentaC));
        dolar.setOnMouseClicked(e -> monedaDolar(dolarC));
        
        monedas.getChildren().addAll(cent, cinco, diez, veinticinco, cincuenta, dolar);
        
        
    }
    
    
    private ImageView imagenesMonedas(String url, double size){
        
        Image centavo = new Image(getClass().getResource(url).toExternalForm());

        ImageView centavo2 = new ImageView();
        centavo2.setFitHeight(size);
        centavo2.setFitWidth(size);
        centavo2.setImage(centavo);
        return centavo2;
        
    }
    
    
   private void imagenes2 (HBox alcancia){
       
       ImageView martillo= imagenesMonedas("/imagenes/martillo.png",250);
       chancho= imagenesMonedas("/imagenes/chancho.png",400);
       
        martillo.setOnMouseClicked(e -> martillo());

       VBox total1= new VBox();
       Label totalAhorro= new Label("TOTAL AHORROS:   ");
       Label romperAlcancia = new Label("Romper Alcancia");
       
       HBox totalAhorro2 = new HBox();
       totalAhorro2.getChildren().addAll(totalAhorro, ahorro);
       total1.getChildren().addAll(totalAhorro2,romperAlcancia, martillo);
       
       alcancia.getChildren().addAll(chancho, total1);
       
       
   }
   
   public BorderPane ventana(){
     
       return ventana;
   }
   
   
   private void sumarMoneda(Label l){
     Integer count = Integer.parseInt(l.getText());
     l.setText(Integer.toString(++count));
     
     sumarTotal();
   }
   
   
   private void sumarTotal(){
       dineroTotal = 0.0;
       double[] valores = {0.01, 0.05, 0.1,0.25,0.5,1.0};
       int indice = 0;
       for(Entry e: monedaMapa.entrySet()){
           Moneda m = (Moneda)e.getKey();
           dineroTotal += m.getValor() * (int)e.getValue();
           indice++;
       }
       
//       for(Node n: cantidadMoneda.getChildren()){
//           Label l = (Label)n;
//           dineroTotal += valores[indice] * Integer.parseInt(l.getText());
//           indice++;
//       }
       Math.round(dineroTotal);
       
       
        ahorro.setText("$ " + Double.toString(dineroTotal));
       
       
   }
   
   
   private void monedaCentavo(Label l){
       
       
       
        Stage secondaryStage = new Stage();
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Diálogo de Confirmación");
        alert.setContentText("¿ Seguro que desea agregar dinero ?");
        Optional<ButtonType> result = alert.showAndWait();
        
        if (result.get() == ButtonType.OK){
            
            Integer count = Integer.parseInt(l.getText());
            l.setText(Integer.toString(++count));
     
            monedaMapa.put(monedasArreglo[0], monedaMapa.get(monedasArreglo[0]) 
                    + 1);
            sumarTotal();
            
        } 
        else{
            
            alert.close();
        }
        
     //inicializarValores();

   }
   private void monedaCinco(Label l){
       Stage secondaryStage = new Stage();
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Diálogo de Confirmación");
        alert.setContentText("¿ Seguro que desea agregar dinero ?");
        Optional<ButtonType> result = alert.showAndWait();
        
        if (result.get() == ButtonType.OK){
            
            Integer count = Integer.parseInt(l.getText());
            l.setText(Integer.toString(++count));

            monedaMapa.put(monedasArreglo[1], monedaMapa.get(monedasArreglo[1]) 
                    + 1);
            sumarTotal();
            
        } 
        else{
            
            alert.close();
        }
     
     
   }
   private void monedaDiez(Label l){
       Stage secondaryStage = new Stage();
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Diálogo de Confirmación");
        alert.setContentText("¿ Seguro que desea agregar dinero ?");
        Optional<ButtonType> result = alert.showAndWait();
        
        if (result.get() == ButtonType.OK){
            
            Integer count = Integer.parseInt(l.getText());
            l.setText(Integer.toString(++count));

            monedaMapa.put(monedasArreglo[2], monedaMapa.get(monedasArreglo[2]) 
                    + 1);
            sumarTotal();
            
        } 
        else{
            
            alert.close();
        }
     
   }
   private void monedaVeinticinco(Label l){
     Stage secondaryStage = new Stage();
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Diálogo de Confirmación");
        alert.setContentText("¿ Seguro que desea agregar dinero ?");
        Optional<ButtonType> result = alert.showAndWait();
        
        if (result.get() == ButtonType.OK){
            
            Integer count = Integer.parseInt(l.getText());
            l.setText(Integer.toString(++count));

            monedaMapa.put(monedasArreglo[3], monedaMapa.get(monedasArreglo[3]) + 1);
            sumarTotal();
            
        } 
        else{
            
            alert.close();
        }
     
   }
   private void monedaCincuenta(Label l){
       Stage secondaryStage = new Stage();
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Diálogo de Confirmación");
        alert.setContentText("¿ Seguro que desea agregar dinero ?");
        Optional<ButtonType> result = alert.showAndWait();
        
        if (result.get() == ButtonType.OK){
            
            Integer count = Integer.parseInt(l.getText());
            l.setText(Integer.toString(++count));

            monedaMapa.put(monedasArreglo[4], monedaMapa.get(monedasArreglo[4]) 
                    + 1);
            sumarTotal();
            
        } 
        else{
            
            alert.close();
        }
     
   }
   private void monedaDolar(Label l){
       Stage secondaryStage = new Stage();
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Diálogo de Confirmación");
        alert.setContentText("¿ Seguro que desea agregar dinero ?");
        Optional<ButtonType> result = alert.showAndWait();
        
        if (result.get() == ButtonType.OK){
            
            Integer count = Integer.parseInt(l.getText());
            l.setText(Integer.toString(++count));

            monedaMapa.put(monedasArreglo[5], monedaMapa.get(monedasArreglo[5]) 
                    + 1);
            sumarTotal();
            
        } 
        else{
            
            alert.close();
        }
     
   }
   
   
    public void martillo(){
       
        Stage secondaryStage = new Stage();
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Diálogo de Confirmación");
        alert.setContentText("¿ Seguro que desea romper ?");
        Optional<ButtonType> result = alert.showAndWait();
        
        if (result.get() == ButtonType.OK){
            
            Runnable romper = new Runnable() {
            @Override
            public void run() {
                //chancho.setImage(new Image(getClass().getResource("").toExternalForm()));
                for(int i=1;i<5;i++){
                 chancho.setImage(new Image(getClass().getResource("/imagenes/romper"+i+".png").toExternalForm()));
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        };
        Thread romperAlcancia = new Thread(romper);
        romperAlcancia.start();
        ahorro.setText("$0.0");
        centavoC.setText("0");
        veinticincoC.setText("0");
        diezC.setText("0");
        cincuentaC.setText("0");
        cincoC.setText("0");
        dolarC.setText("0");
        for(Entry e: monedaMapa.entrySet()){
           monedaMapa.replace((Moneda)e.getKey(), 0);
        }
            
        } 
        else{
            
            alert.close();
        }
        
        
   }
    
    public static void guardar() throws FileNotFoundException, IOException{
        FileOutputStream fos = new FileOutputStream("archivoBinario.dat");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(monedaMapa);
    }
   
    private Map<Moneda,Integer> leer() throws IOException, ClassNotFoundException{
        FileInputStream fis = new FileInputStream("archivoBinario.dat");
        ObjectInputStream ois = new ObjectInputStream(fis);
        return (Map<Moneda,Integer>)ois.readObject();
    }
    
    private void inicializarValores(){
        try {
            monedaMapa = leer();
            int i = 0;
            for(Entry e: monedaMapa.entrySet()){
                monedasArreglo[i] = (Moneda)e.getKey();
                i++;
            }
            
            centavoC.setText(Integer.toString(monedaMapa.get(monedasArreglo[0])));
            veinticincoC.setText(Integer.toString(monedaMapa.get(monedasArreglo[1])));
            diezC.setText(Integer.toString(monedaMapa.get(monedasArreglo[2])));
            cincuentaC.setText(Integer.toString(monedaMapa.get(monedasArreglo[3])));
            cincoC.setText(Integer.toString(monedaMapa.get(monedasArreglo[4])));
            dolarC.setText(Integer.toString(monedaMapa.get(monedasArreglo[5])));
            
            sumarTotal();
            
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
            
}
