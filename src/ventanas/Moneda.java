/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ventanas;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author 
 */
public class Moneda implements Serializable{
    
    double valor;
    String nombre;
    
    
    public Moneda(Double valor, String nombre){
        this.valor=valor;
        this.nombre=nombre;
        
        
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public Double getValor(){
        return valor;
    }
    
    public void setValor(Double valor){
        this.valor=valor;
        
    }
    
    @Override
    public int hashCode(){
        
        return (int) valor;
        
    }
    
    @Override
    public boolean equals(Object o){
        if(o==null){
            return false;
            
        }
        if(getClass()!= o.getClass()){
            return false;
            
        }
        final Moneda other =(Moneda) o;
        if(this.valor!= other.valor){
            return false;
            
        }
        return !Objects.equals(this.nombre, other.nombre);
    }

    
    
    
    
}
